$(document).ready(function(){

    var burgerTag = $('.burger');
    var navMobile = $('.nav-mobile');

    burgerTag.on('click', function(){
        navMobile.toggleClass('nav-visible');

        if(navMobile.hasClass('nav-visible')){
            burgerTag.attr('src', '../Imagens/small_close.svg')
        } else{
            burgerTag.attr('src', '../Imagens/burger.svg')
        }
    })
})